import os
from flask import Flask, render_template

app = Flask(__name__)

# main
@app.route('/', defaults={'req_path': ''})
@app.route('/<path:req_path>')
def browse_folder(req_path):
    # change this to the path of your choice
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))

    # set parent url
    parent_url = '/'.join(req_path.split('/')[:-1])

    # get path
    abs_path = os.path.join(BASE_DIR, req_path)

    # return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return render_template('404.html', folder=req_path)

    # check if path is a file and serve
    if os.path.isfile(abs_path):
        contents = None
        with open(abs_path, 'rb') as f:
            contents = f.read()

        return render_template('view.html', file=os.path.basename(req_path), contents=contents, top=parent_url)

    # get directory listing
    folders = []
    files = []
    for file in sorted(os.listdir(abs_path), key=os.path.basename):
        # ignore files that starts with '.'
        if file[0]=='.':
            continue

        is_file = os.path.isfile(os.path.join(abs_path, file))
        obj = {}
        obj[file] = 'file' if is_file else 'folder'

        if is_file:
            files.append(obj)
        else:
            folders.append(obj)

    return render_template('browse_folder.html', folder=req_path, files=folders+files, top=parent_url)

if __name__ == '__main__':
    app.debug = True
    app.run(port=5000)
